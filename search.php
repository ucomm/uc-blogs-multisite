<?php 
    get_header(); 
    $total_posts = UCBlogs\Lib\Helpers::get_total_posts(); 
?>

    <main id="content" role="main" aria-label="Content">
        <section class="section-wrapper">
            <?php get_search_form(); ?>
            <h1><?php echo sprintf( __( '%s Search Results for ', 'uc-blogs' ), $wp_query->found_posts ); echo get_search_query(); ?></h1>
            <?php require_once(UC_BLOGS_DIR . '/partials/posts-container.php'); ?>
        </section>
    </main>

<?php get_footer(); ?>