<?php
    use A11y\Menu_Walker;

    $posts_per_page = get_option('posts_per_page');
    $posts_count = wp_count_posts();
    $total_posts = $posts_count->publish;
?>

<!DOCTYPE html>
<html <?php language_attributes(); ?> class="no-js">
<head>
    <?php 
        // enable the "custom" container code placement option for GTM plugin
        if ( function_exists( 'gtm4wp_the_gtm_tag' ) ) { gtm4wp_the_gtm_tag(); } 
    ?>
    <meta charset="<?php bloginfo( 'charset' ); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">    
    <link rel="profile" href="http://gmpg.org/xfn/11" />
    <link rel="pingback" href="<?php bloginfo('pingback_url'); ?>" />
    <title><?php wp_title( '' ); ?><?php if ( wp_title( '', false ) ) { echo ' : '; } ?><?php bloginfo( 'name' ); ?></title>
    <?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
    <!-- Privacy Policy / Cookie Notification -->
    <script type='text/javascript' src='https://ucommobjectstorage.blob.core.windows.net/cookie-jar/cookie-notification.js'></script>
    <noscript><p>Our websites may use cookies to personalize and enhance your experience. By continuing without changing your cookie settings, you agree to this collection. For more information, please see our <a href="https://privacy.uconn.edu/university-website-notice/" target="_blank">University Websites Privacy Notice</a>.</p></noscript>
    <div class="wrapper">
        <header>
            <?php if(current_user_can( 'manage_options' )): ?>
                <div style="position:fixed;width:100vw;color:#fff;background-color:rgba(0,0,0,.8);z-index:1000;bottom:0;padding:1em;">Theme Template: <?php global $template; echo basename($template); ?></div>
            <?php endif; ?>

            <?php
                get_template_part('partials/banner');
                $site_name = get_bloginfo('name');        
            ?>
            <div class='sub-header'>
                <a class='skip-link' href="#content">Skip to content</a>
                <div class="site__name-wrapper">
                    <p>
                        <a href="<?php echo get_option('home'); ?>" class='site-name'><?php echo $site_name; ?></a>
                    </p>
                    <button id="menu-icon" class="hamburger hamburger--spin" type="button">
                        <span class="hamburger-label">Menu</span>
                        <span class="hamburger-box">
                            <span class="hamburger-inner"></span>
                        </span>
                    </button>
                </div>
                
                <?php
                    $items_wrap = '<ul id="%1$s" class="am-click-menu %2$s">%3$s</ul>';
                    $args = array(
                        'container' => 'nav',
                        'container_id' => 'am-menu-container',
                        'items_wrap' => $items_wrap,
                        'menu_id' => 'am-main-menu',
                        'theme_location' => 'main',
                        'walker' => new Menu_Walker(true)
                    );
                    wp_nav_menu($args);
                ?>
            </div>
        </header>
    