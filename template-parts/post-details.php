
<?php

foreach ($posts as $post) {
  $excerpt = UCBlogs\Lib\Helpers::create_excerpt($post->ID);
?>
  <li class='site-list-item'>
    <?php
      if (!$main_site) {
        $date = get_the_date('F d, Y', $post->ID);
        $date_time = get_the_date('Y-m-d', $post->ID);
    ?>
        <time datetime="<?php echo $date_time; ?>" class="site-list_date">
          <?php echo $date; ?>
        </time>
    <?php
      }
    ?>
    <div class='site-list-image--container'>
      <a aria-hidden="true" href="<?php the_permalink($post->ID); ?>" class="site-list-image_link">
      <?php
        if (!has_post_thumbnail($post->ID)) {
          echo "<img alt='' class='thumbnail-fallback attachment-thumbnail-card size-thumbnail-card wp-post-image' src='" . UC_BLOGS_URL . '/img/uconn-placeholder-3-2.jpg' . "' />";
        } else {
          $thumbnail = get_the_post_thumbnail($post->ID, 'thumbnail-card', true);
          echo $thumbnail;
        }
      ?>
      </a>
    </div>
    <h3 class='site-list-title'>
      <a href="<?php the_permalink($post->ID); ?>" class="site-list-title_link">
          <?php echo $post->post_title; ?>
      </a>
    </h3>
    <p class='site-list-excerpt'><?php echo $excerpt; ?></p>
    <a class='site-list-link' href="<?php the_permalink($post->ID) ?>"
      aria-hidden="true">Read more</a>
  </li>
<?php
}

?>




