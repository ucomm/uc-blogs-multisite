<?php
$args = array(
  'numberposts' => 1,
  'post_status' => 'publish'
);
$first_post = get_posts($args)[0];
$date = get_the_date('F d, Y', $first_post->ID);
$date_time = get_the_date('Y-m-d', $first_post->ID);
$excerpt = UCBlogs\Lib\Helpers::create_excerpt($first_post->ID);
?>
<div class="first-post_container">
  <time datetime="<?php echo $date_time; ?>" class="site-list_date-small">
        <?php echo $date; ?>
  </time>
  <div class="first-post_image--container">
    <a aria-hidden="true" href="<?php the_permalink($first_post->ID); ?>" class="site-list-image_link">
    <?php
      if (!has_post_thumbnail($first_post->ID)) {
        echo "<img alt='' class='thumbnail-fallback attachment-thumbnail-card size-thumbnail-card wp-post-image' src='" . UC_BLOGS_URL . '/img/uconn-placeholder-3-2.jpg' . "' />";
      } else {
        $thumbnail = get_the_post_thumbnail($first_post->ID, 'thumbnail-card', true);
        echo $thumbnail;
      }
    ?>
    </a>
  </div>
  <div class="first-post_content">
    <time datetime="<?php echo $date_time; ?>" class="site-list_date">
          <?php echo $date; ?>
    </time>
    <h2 class="first-post_title">
      <a href="<?php the_permalink($first_post->ID); ?>" class="site-list-title_link">
          <?php echo $first_post->post_title; ?>
      </a>
    </h2>
    <p class="first-post_excerpt">
        <?php echo $excerpt; ?>
    </p>
    <a class='first-post_link' 
      href="<?php the_permalink($first_post->ID) ?>"
      aria-hidden="true">
      Read more
    </a>
  </div>
</div>