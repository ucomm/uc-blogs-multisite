<article id="content-none">
    <h1><?php esc_html_e( 'No results.', 'uc-blogs' ); ?></h1>

    <p><a href="<?php echo esc_url( home_url() ); ?>"><?php esc_html_e( 'Return to the homepage.', 'uc-blogs' ); ?></a><p>
</article>