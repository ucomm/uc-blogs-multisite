#!/bin/bash

# use this script to generate the banner if needed

# change directories to the banner
cd ./vendor/uconn/banner

# install its dependencies including the dev dependencies
composer install

# generate the banner
php ./src/Banner/Generator.php -p ../../../partials/banner.php

# remove whitespace from the file
sed -i '' '/^[[:space:]]*$/d' ../../../partials/banner.php

cp _site/banner.css ../../../assets/css/banner.css

cd ../../../

# prepend the banner styles with a comment
echo "/* ATTENTION: only update this banner using the banner.sh generator script */" | cat - ./assets/css/banner.css > temp && mv temp ./assets/css/banner.css

# prepend the file with a php tag and comment
echo "<?php // ATTENTION: only update this banner using the banner.sh generator script ?>" | cat - ./partials/banner.php > temp && mv temp ./partials/banner.php