<?php 
    get_header();
    require_once(UC_BLOGS_DIR . '/partials/breadcrumbs.php');

    $date = get_the_date();
    $date_time = get_the_date('Y-m-d');

?>

    <main id="content" role="main" aria-label="Content">
        <?php 
            if ( have_posts() ) : while ( have_posts() ) : the_post(); 
                $prev = get_previous_post();
                $next = get_next_post();

            
                if (!empty($prev) && $prev->ID) {
                    $prev_link = get_permalink($prev->ID);
                    ?>
                    <a aria-label="go to the previous post" class="prev-link" href="<?php echo $prev_link; ?>"><i class="fas fa-chevron-left"></i></a>
                    <?php
                }
                
                if (!empty($next) && $next->ID) {
                    $next_link = get_permalink($next->ID);
        ?>
                    <a aria-label="go to the next post" class="next-link" href="<?php echo $next_link; ?>"><i class="fas fa-chevron-right"></i></a>
        <?php
                }
                
        ?>

        <section <?php 
            if (!FLBuilderModel::is_builder_enabled()) {
                echo "class='post-wrapper'";
            }
        ?>>
            <?php
                if (FLBuilderModel::is_builder_enabled()) {
                    echo "<div class='post-wrapper_extra'>";
                }
            ?>
                <time datetime="<?php echo $date_time; ?>">
                    <?php echo $date; ?>
                </time>
                <h1 class="post-title"><?php the_title(); ?></h1>
                <div class="post-featured-image">
                <?php
                    if (!has_post_thumbnail($post->ID)) {
                    echo "<img class='thumbnail-fallback attachment-thumbnail-card size-thumbnail-card wp-post-image' src='" . UC_BLOGS_URL . '/img/uconn-placeholder-3-2.jpg' . "' />";
                    } else {
                    $thumbnail = get_the_post_thumbnail($post->ID, 'thumbnail-card', true);
                    echo $thumbnail;
                    }
                ?>
                </div>
                <?php
                    if (FLBuilderModel::is_builder_enabled()) {
                        echo "</div>";
                    }
                ?>
                    <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                        <?php 
                        the_content();
                        $author_data = UCBlogs\Lib\Helpers::get_author_data();
                        $full_name = $author_data['first_name'] . " " . $author_data['last_name'];
                        ?>
                        <div class="post-social-container post-wrapper_extra">
                            <p>
                                <strong>Posted by</strong> <a href="<?php echo $author_data['url']; ?>">
                                <?php echo $full_name; ?>
                                </a>
                            </p>
                            <p><?php the_tags(); ?></p>
                        </div>
                    </article>
                </section>

        <hr class="util-wrapper"/>
        <section class="post-wrapper">
                <div class="post-author-container">
                    <div class="author__details-image"
                        style="background-image: url(<?php echo $author_data['headshot']; ?>);
                        background-position: center;
                        background-repeat: no-repeat;
                        background-size: cover;"></div>
                    <div class="author__details-content">
                        <p><strong>About <?php echo $full_name; ?></strong></p>
                        <p><?php echo $author_data['description']; ?></p>
                        <a href="<?php echo $author_data['url']; ?>">
                            View all of <?php echo $author_data['first_name']; ?>'s posts.
                        </a>
                    </div>
                </div>
            <?php endwhile; ?>

            <?php else : ?>
                <?php get_template_part('template-parts/content', 'none'); ?>
            <?php endif; ?>
        </section>
    </main>

<?php get_footer(); ?>