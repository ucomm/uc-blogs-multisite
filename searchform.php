<!-- search -->
<form id="search-form" role="search" class="search" method="get" action="<?php echo esc_url( home_url() ); ?>">
    <label for="search-input">Search Blogs</label>
    <input id="search-input" class="search-input" type="text" name="s" aria-label="Search site for:" placeholder="<?php esc_html_e( 'To search, type and hit enter.', 'uc-blogs' ); ?>">
    <input id="search-submit" type="submit" class="search-submit" value="<?php esc_html_e('Search', 'uc-blogs'); ?>" />
</form>
<!-- /search -->