<?php
namespace UCBlogs\Lib;

use ScssPhp\ScssPhp\Compiler;

class Customizer {
  static public function customize_register($wp_customize) {

    // Panel
    $wp_customize->add_section('ucblogs', array(
      'title' => __('UConn Blogs', 'uc-blogs'),
      'priority' => 10,
      'description' => __('Use this to customize the highlight color of the sub site')
    ));

    // Settings
    $wp_customize->add_setting('ucblogs_accent', array(
      'default' => '#ffcd00',
      'sanitize_callback' => 'sanitize_hex_color'
    ));

    $wp_customize->add_control('ucblogs_accent', array(
      'label' => __('Accent Color', 'uc-blogs'),
      'section' => 'ucblogs',
      'settings' => 'ucblogs_accent',
      'type' => 'select',
      'default' => '#ffcd00',
      'choices' => array(
        '#ffcd00' => __('Yellow', 'uc-blogs'),
        '#ee6921' => __('Orange', 'uc-blogs')
      )
    ));
  }
  static public function compile_scss($customizer) {
    $path = UC_BLOGS_DIR . '/src/customizer_scss/';
    $scss = new Compiler();
    $ucblogs_accent = get_theme_mod('ucblogs_accent', '#ffcd00');
    $scss_vars = array(
      'ucblogs_accent' => $ucblogs_accent
    );
    $scss->setVariables($scss_vars);
    // update_option('scss_vars', $scss->getVariables($scss_vars)) works.
    $scss->setImportPaths($path);
    $scss->setFormatter('ScssPhp\ScssPhp\Formatter\Compressed');
    update_option('path', $path);
    // the following lines don't work.
    // $path is stored correctly in the db.
    // however $scss->getParsedFiles() stores an empty array
    // and the compile method doesn't create any css.
    update_option('parsed', $scss->getParsedFiles());
    $compiled = $scss->compile('@import "app.scss";');

    if (!get_option('customizer_css')) {
      add_option('customizer_css');
    }
    update_option('customizer_css', $compiled);
  }
  static public function print_css() {
    $css = get_option('customizer_css');
    echo "<style id='customizer_css'>$css</style>";
  }
}