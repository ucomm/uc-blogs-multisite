<?php

namespace UCBlogs\Lib;
/**
 * This class handles displaying posts from across the multisite blog network.
 */
class ShortcodeHandler {
  /**
   * Displays posts on a per site basis.
   * If used on the main site of the network, the 'site' attribute is required. Otherwise the site is inferred.
   *
   * @param array $atts - attributes that are added to the shortcode
   * @param string $content - the post content
   * @return string $output - HTML markup
   */
  static public function show_posts($atts, $content = null) {
    $html = '';
    $a = shortcode_atts(array(
      'alternate_style' => false,
      'site' => '',
      'posts_per' => 3
    ), $atts);
    $main_site = is_main_site();
    $alternate_style = filter_var($a['alternate_style'], FILTER_VALIDATE_BOOLEAN, FILTER_NULL_ON_FAILURE);
    $posts_per_page = $a['posts_per'];
    $offset = 0;

    // if not on the main site, only use stories from the current blog.
    if (!$main_site) {
      $blog_id = get_current_blog_id();
      $blog_details = get_blog_details($blog_id);
      // if using the "regular" style, prepare for the first post to be different.
      if (!$alternate_style) {
        $offset = 1;
      }
    } else {
      $blog_details = get_blog_details($a['site']);
      $blog_id = $blog_details->blog_id;
      switch_to_blog($blog_id);
    }

    $args = array(
      'offset' => $offset,
      'posts_per_page' => $posts_per_page,
      'post_status' => 'publish'
    );
    $posts = get_posts($args);

    $output = ob_start();

    // require a specific site on the main site.
    if ($main_site && !$a['site']) {
      echo "<p><strong>Please choose a site by name, slug, or ID</strong></p>";
      $output = ob_get_clean();
      return $output;
    }

    // enforce using the alternate style only on sub-sites.
    if ($main_site && $alternate_style) {
      echo "<p><strong>Please only use the alternate style on sub-sites.</strong></p>";
      $output = ob_get_clean();
      return $output;
    }

    // fallback if the site requested doesn't exist.
    if (!$blog_details) {
      echo "<p><strong>Please choose a valid site within the network.</strong></p>";
      $output = ob_get_clean();
      return $output;
    }

    // all the shortcode attributes are ok
    // create the posts.
    echo "<div class='site-posts_container'>";
    if (!$main_site && !$alternate_style) {
      require(UC_BLOGS_DIR . '/template-parts/first-post.php');
      echo "<hr />";
    } else {
      echo "<h2 class='site__posts-title'>$blog_details->blogname</h2>";
    }
    echo "<ul id='posts-list-$blog_id' class='site__posts-list'>";
    require(UC_BLOGS_DIR . '/template-parts/post-details.php');

    // close out the <ul> and the container <div>
    if (!$main_site && $alternate_style) {
      echo "</ul></div>";
    } else if ($main_site) {
      echo "
          </ul>
          <a class='site-link' href='$blog_details->siteurl'>Read more about $blog_details->blogname</a>
        </div>";
    } else {
      echo "
          </ul>
          <button class='more-posts'>More Posts</button>
        </div>
      ";
    }
    
    $output = ob_get_clean();

    if ($main_site) {
      // reset the blog query
      restore_current_blog();
    }
    return $output;
  }
  /**
   * The function called by wp_ajax_load_posts and wp_ajax_nopriv_load_posts when an AJAX request is made.
   * See /src/js/index.js for the JS part of the implementation
   * 
   * Fetches the appropriate template and returns valid HTML as a string
   * 
   * @return string - HTML from a template file
   */
  public static function load_posts() {
    $current_length = (int)$_POST['current_length'];
    $posts_per_page = get_option('posts_per_page');
    $posts_count = wp_count_posts();
    $total_posts = (int)$posts_count->publish;
    $args = array(
      'offset' => $current_length,
      'posts_per_page' => $posts_per_page,
      'post_status' => 'publish'
    );
    $posts = get_posts($args);
    if ($current_length >= $total_posts) {
      return null;
    } else {
      require(UC_BLOGS_DIR . '/template-parts/post-details.php');
    }
    wp_die();
  }
}