<?php

namespace UCBlogs\Lib;

class Helpers {
    public static function add_headshot($user) {
        require_once(UC_BLOGS_DIR . '/partials/admin-custom-image.php');
    }
    public static function save_additional_user_meta($user_id) {
        if (!current_user_can('edit_user', $user_id)) return;

        update_user_meta($user_id, 'user_meta_image', $_POST['user_meta_image']);
        update_user_meta($user_id, 'user_meta_image_name', $_POST['user_meta_image_name']);
        update_user_meta($user_id, 'user_meta_image_id', $_POST['user_meta_image_id']);
    }
    /**
     * Adds Proxima Nova fonts to Beaver Builder
     */
    public static function add_custom_fonts() {
        $custom_fonts = array(
            'Proxima Nova' => array(
                'fallback' => 'Verdana, Arial, Helvetica, sans-serif',
                'weights' => array(
                    '300',
                    '400',
                    '600',
                    '700'
                )
            )
        );
        foreach ($custom_fonts as $font => $settings) {
            if (class_exists('FLFontFamilies') && isset(\FlFontFamilies::$system)) {
                \FLFontFamiles::$system[$font] = $settings;
            }
            if (class_exists('FLBuilderFontFamilies') && isset(\FLBuilderFontFamilies::$system)) {
                \FLBuilderFontFamilies::$system[$font] = $settings;
            }
        }
    }
    public static function add_mime_types($mimes = array()) {
        $mimes['zip'] = 'application/zip';
        $mimes['svg'] = 'image/svg+xml';
        return $mimes;
    }
    /**
     * Generate an excerpt from a post if it doesn't exist.
     *
     * @param int $id the post ID
     * @return string - the generated excerpt
     */
    public static function create_excerpt($id) {
        $excerpt = '';
        if (has_excerpt($id)) {
            $excerpt = get_the_excerpt($id);
        } else {
            $raw_excerpt = get_post_field('post_content', $id);

            // if the post starts with a code block, remove it.
            $raw_excerpt = preg_replace('/<pre(.*?)>(.*)<\/pre>/si', '', $raw_excerpt);
            
            $excerpt = wp_trim_words($raw_excerpt, 25);
        }
        return $excerpt;
    }
    /**
     * Aggregate details about authors from the database.
     *
     * @return array - author details in an array
     */
    public static function get_author_data() {
        return array(
            'id' => get_the_author_meta('ID'),
            'url' => get_author_posts_url(get_the_author_meta('ID')),
            'first_name' => get_the_author_meta('first_name'),
            'last_name' => get_the_author_meta('last_name'),
            'description' => get_the_author_meta('description'),
            'headshot' => get_the_author_meta('user_meta_image')
        );
    }
    /**
     * Undocumented function
     *
     * @return void
     */
    public static function get_total_posts() {
        $posts_per_page = get_option('posts_per_page');
        $posts_count = wp_count_posts();
        return $posts_count->publish;
    }
    /**
     * Enqueue stylesheets and scripts.
     */
    public static function load_scripts() {
        $wp_env = wp_get_environment_type();
        $asset_dir = $wp_env !== 'production' ? '/build' : '/dist';
        $is_builder_active = False;
        if(class_exists("FLBuilderModel")){
            $is_builder_active = \FLBuilderModel::is_builder_active();
        }
        wp_enqueue_style('font-awesome-5', 'https://use.fontawesome.com/releases/v5.2.0/css/all.css');

        wp_enqueue_style('a11y-menu-styles', UC_BLOGS_URL . '/vendor/ucomm/a11y-menu/dist/main.css');        
        wp_enqueue_style('uc-blogs-styles', UC_BLOGS_URL . $asset_dir .'/main.css');
        wp_enqueue_style('uconn-banner', UC_BLOGS_URL . '/assets/css/banner.css');

        if (!is_plugin_active('font-awesome/index.php') && !$is_builder_active) {
            wp_enqueue_style('font-awesome-6', OBJECT_STORAGE_FONTAWESOME_URL . '/all.min.css');
            wp_enqueue_style('font-awesome-shim', 'https://use.fontawesome.com/releases/v6.4.2/css/v4-shims.css', ['font-awesome-6']);
        }

        wp_register_script('a11y-menu', UC_BLOGS_URL . '/vendor/ucomm/a11y-menu/dist/Navigation.js', array(), false, true);
        wp_enqueue_script('a11y-menu');

        // enqueue assets for mmenu only when BB is not active.
        // this resolves a bug with BB in the responsive design mode.
        if (!$is_builder_active) {
            wp_enqueue_script('mmenu', UC_BLOGS_URL . '/mmenu/jquery.mmenu.all.js', array('jquery'), false, false);
            wp_enqueue_style('mmenu', UC_BLOGS_URL . '/mmenu/jquery.mmenu.all.css');
        }
        
        wp_enqueue_script('uc-blogs-script', UC_BLOGS_URL . $asset_dir . '/index.js', array('jquery', 'a11y-menu'), false, true);

        wp_localize_script('uc-blogs-script', 'ajax_obj', array(
            'ajaxurl' => admin_url('admin-ajax.php'),
            'is_main_site' => is_main_site(),
            'is_builder_active' => $is_builder_active
        ));

        if (is_post_type_archive("directory")){
            wp_enqueue_script('social_media_dir_script', UC_BLOGS_URL . '/src/js/SocMedDir.js', array('jquery'), false, true);
        }
    }
    public static function load_admin_scripts() {
        $screen = get_current_screen();
        if ($screen->base === 'profile') {
            wp_enqueue_media();
        }
        wp_enqueue_script('admin', UC_BLOGS_URL . '/src/admin-js/admin.js', array('jquery'), false, true);
    }
    /**
     * Register theme menus
     *
     */
    public static function register_menus() {
        register_nav_menu('main', __('Main Menu', 'uc-blogs'));
        register_nav_menu('footer', __('Footer Menu', 'uc-blogs'));
        register_nav_menu('footer-site-list', __('Footer Site List', 'uc-blogs'));
    }
    /**
     * Register widget areas.
     */
    public static function register_widgets() {
        register_sidebar(array(
            'name' => __('Footer Widget 1', 'uc-blogs'),
            'id' => 'footer_widget_1',
            'before_widget' => '<div class="widget footer-widget">',
            'after_widget' => '</div>'
        ));
        register_sidebar(array(
            'name' => __('Footer Widget 2', 'uc-blogs'),
            'id' => 'footer_widget_2',
            'before_widget' => '<div class="widget footer-widget">',
            'after_widget' => '</div>'
        ));
    }
    public static function add_mobile_menu() {
        get_template_part('partials/mobile-nav');
    }
    /**
     * Detect if 'wordpress' is part of the home url in wp_options -> home
     * If so, remove it by setting the url directly to the server host.
     * similar approaches can be used with other options
     * See here for documentation -  https://codex.wordpress.org/Plugin_API/Filter_Reference/option_(option_name)
     *
     * NB - this doesn't actually change the DB. 
     * it just changes the result of  the url when it hits the server.
     *
     * @param [string] $url - the url stored at wp_options -> home
     * @return $url
     */
    public static function update_siteurl($url) {
        if (strpos($url, 'wordpress')) {
            $url = 'http://' . $_SERVER['HTTP_HOST'];
        }
        return $url;
    }

    public static function get_social_icon($type) {
        if (!is_plugin_active('font-awesome/index.php')) {
            return file_get_contents(OBJECT_STORAGE_FONTAWESOME_URL . '/svgs/brands/' . $type . '.svg');
        } else {
            return do_shortcode('[icon prefix="fab" name="' . $type . '" class="icon-' . $type . ' social-icon server-icon"]');
        }
    }
}