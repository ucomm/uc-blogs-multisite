<?php
namespace UCBlogs\Lib;
/**
 * 
 * This is a utility class to bootstrap sites and posts for local development.
 * It is NOT for production... at all...ever.
 * 
 */
class Bootstrapper {
  /**
   * A getter to check the current server hostname of the theme.
   *
   * @return void
   */
  public function get_server_host() {
    return $_SERVER['HTTP_HOST'];
  }
  /**
   * A getter to check if the bootstrapping process has been run or not.
   *
   * @return void
   */
  public function get_uc_blogs_setup() {
    return get_option('uc_blogs_setup');
  }
  /**
   * bootstrap_posts() creates posts for each site using json template files.
   *
   * @return void
   */
  public function bootstrap_posts() {

    $sites = get_sites();

    foreach ($sites as $site) {
      switch_to_blog($site->blog_id);
      $blog_details = get_blog_details($site->blog_id);
      $blog_path = $blog_details->path;
      $blog_slug = str_replace('/', '', $blog_path);

      if ($blog_slug !== "") {
        $posts_path = UC_BLOGS_DIR . "/utils/post-seeds/$blog_slug.json"; 
        $posts = file_get_contents($posts_path);
        $posts = json_decode($posts);
        foreach ($posts as $index => $post) {
          $args = array(
            'post_content' => $post->content,
            'post_date' => $post->date,
            'post_title' => $post->title,
            'post_status' => 'publish'
          );
          wp_insert_post($args);
        }
      }
      // delete the default Hello World WP post
      wp_delete_post(1);
      restore_current_blog();
    }

    update_option('uc_blogs_setup', true);
  }
  /**
   * bootstrap_sites() creates a set of 3 blog sites on a multisite network.
   * they are created with the account used to initialize the network.
   *
   * @return void
   */
  public function bootstrap_sites() {
    if ($_SERVER['HTTP_HOST'] !== 'localhost') return;

    $sites = array(
      'social' => array(
        'path' => '/social',
        'title' => 'UConn Social'
      ),
      'dev' => array(
        'path' => '/web-development',
        'title' => 'UConn Web Development'
      ),
      'design' => array(
        'path' => '/web-design',
        'title' => 'UConn Web Design'
      )
    );
    foreach ($sites as $site_info) {
      $domain = 'localhost';
      $path = $site_info['path'];
      $title = $site_info['title'];

      wpmu_create_blog($domain, $path, $title, 1);

    }
  }
  /**
   * This is the method to call in functions.php to bootstrap the sites and posts.
   *
   * @return void
   */
  public function init() {
    if (!get_option('uc_blogs_setup')) {
      add_option('uc_blogs_setup', false);
    }
    $this->bootstrap_sites();
    $this->bootstrap_posts();
  }
  private function get_menu_id($menu_name) {
    // $menu_name = 'main-menu';
    $menu_obj = wp_get_nav_menu_object($menu_name);
    $menu_id = null;
    if (!$menu_obj) {
      $menu_id = wp_create_nav_menu($menu_name);
    } else {
      $menu_id = $menu_obj->term_id;
    }
    return $menu_id;
  }
  private function create_menu_item($menu_id) {
    
    if (!get_option('uc_blogs_menu_item')) {
      add_option('uc_blogs_menu_item', 0);
    }
    $is_menu_item = get_option('uc_blogs_menu_item', 0);
    wp_update_nav_menu_item($menu_id, $is_menu_item, array(
      'menu-item-title' => __('Blogs', 'uc-blogs'),
      'menu-item-url' => '',
      'menu-item-status' => 'publish'
    ));
    update_option('uc_blogs_menu_item', 1);
  }
  private function create_submenu_items($menu_id) {
    $sites = get_sites();
    $network_url = untrailingslashit(network_site_url());
    array_shift($sites);

    $args = array(
      'name' => 'blogs',
      'post_status' => 'publish'
    );
    $test = get_posts($args);
    echo "<pre>";
    var_dump($test);
    echo "</pre>";


    foreach ($sites as $site) {
      $option = 'uc_blogs_sub_item-' . $site->blog_id;
      $site_url = $network_url . $site->path;
      $menu_title = '';
      if (!get_option($option)) {
        add_option($option, 0);
      }
      $is_sub_item = get_option($option, 0);
      switch_to_blog($site->blog_id);
      $menu_title = get_bloginfo('name');
      restore_current_blog();
      wp_update_nav_menu_item($menu_id, $is_sub_item, array(
        'menu-item-url' => $site_url,
        'menu-item-status' => 'publish',
        'menu-item-title' => $menu_title
      ));
      update_option($option, 1);
    }


  }
  public function create_blog_menu_item($menu_name) {
    $menu_id = $this->get_menu_id($menu_name);
    $this->create_menu_item($menu_id);
    $this->create_submenu_items($menu_id);
  }
}