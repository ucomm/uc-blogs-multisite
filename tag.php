<?php 
    get_header();
    $total_posts = UCBlogs\Lib\Helpers::get_total_posts(); 
    require_once(UC_BLOGS_DIR . '/partials/breadcrumbs.php');
?>

    <main id="content" role="main" aria-label="Content">
        <section class="wrapper section-wrapper">
            <h1>
                Tag:
                <?php
                $tag = get_queried_object();
                echo " " . $tag->name;
                ?>
            </h1>
            <?php 
                require_once(UC_BLOGS_DIR . '/partials/posts-container.php');
            ?>
        </section>
    </main>

<?php get_footer(); ?>