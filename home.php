<?php get_header(); ?>
<main id="content">
  <section class="section-wrapper">
    <?php
      echo do_shortcode('[show_posts]'); 
    ?>
  </section>
</main>
<?php get_footer(); ?>