<div class='site-posts_container'>
  <ul id="posts-list">
    <?php 
      get_template_part('template-parts/content', 'details'); 
    ?>
  </ul>
  <?php
    if ($total_posts > $posts_per_page) {
  ?>
      <button class='more-posts'>More Posts</button>
  <?php
    }
  ?>
</div>