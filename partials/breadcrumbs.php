<?php
$posts_page;
$posts_page_id = get_option('page_for_posts');

if (!$posts_page_id) {
  $pages = get_pages();
  foreach ($pages as $page) {
    $has = has_shortcode($page->post_content, 'show_posts');
    if ($has) {
      $posts_page = $page;
    }
  }

} else {
  $posts_page = get_page($posts_page_id);
}

?>

<nav class="breadcrumb-nav" aria-label="breadcrumb navigation">
  <ol class="breadcrumbs">
    <li class="breadcrumb-list_item">
      <i class="fa fa-home"></i>
      <a class="breadcrumb-list_link" href="<?php echo get_option('home'); ?>"><?php 
        echo get_bloginfo('name'); 
      ?></a>
    </li>
    <li class="breadcrumb-list_item">
      <a class="breadcrumb-list_link" 
        href="<?php the_permalink($posts_page->ID) ?>"><?php 
        echo $posts_page->post_title; 
      ?></a>
    </li>
    <li class="breadcrumb-list_item">
      <a href="#" aria-current="location"><?php
          if (is_single()) {
            the_title();
          }

          if (!is_author()) {
            single_term_title(); 
          } elseif (is_author()) {
            echo $full_name;
          } 
      ?></a>
    </li>
  </ol>
</nav>