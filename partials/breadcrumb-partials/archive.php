<?php
/**
 * Partial used for archive pages of type
 * - category
 * - tag
 * - author
 * - date
 * - CPT
 * - custom tax
 * 
 * as per https://codex.wordpress.org/Function_Reference/is_archive
 * 
 * 
 */
?>

<li class="breadcrumb-list_item">
  <a class="breadcrumb-list_link" href="<?php the_permalink($posts_page->ID) ?>">
    <?php echo $posts_page->post_title; ?>
  </a>
</li>
<li class="breadcrumb-list_item">
  <a href="#">
    <?php single_term_title(); ?>
  </a>
</li>