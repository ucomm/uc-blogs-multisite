<?php
// if the social-pug plugin isn't active, make sure that article meta tags are still included.
if (!is_plugin_active('social-pug/index.php')) {
?>
<meta property="og:url" content="<?php echo $the_post->guid; ?>" />
<meta property="og:type" content="website" />
<meta property="og:title" content="<?php echo $the_post->post_title; ?>" />
<meta property="og:description" content="<?php 
  $excerpt = UCBlogs\Lib\Helpers::create_excerpt($the_post->ID);
  echo $excerpt; 
?>" />
<meta property="og:image" content="<?php the_post_thumbnail_url($the_post->ID); ?>" />
<?php
}
?>
<meta property="og:image:width" content="400" />
<meta property="og:image:height" content="267" />