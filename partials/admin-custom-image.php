<h2><?php _e('Custom User Image', 'uc-blogs')?></h2>
<table class="form-table">
  <tr>
    <th>
      <?php _e('Custom Headshot', 'uc-blogs')?>
    </th>
    <td>
      <img width=600 
        height=400
        style="object-fit: cover; object-position: center;" 
        src="<?php
        $image_url = get_the_author_meta('user_meta_image', $user->ID);
        echo $image_url;
      ?>" />
  </tr>
  <tr>
    <th>
      <label for="uploadimage">Upload image</label>
    </th>
    <td>
      <input type='button'
        id="uploadimage" 
        class="additional-user-image button-primary" 
        value="<?php _e( 'Upload Image', 'textdomain' ); ?>" 
        />
    </td>
  </tr>
  <tr>
    <th>
      <label for="user_meta_image">Custom image URL</label>
    </th>
    <td>
      <input type="text" 
        name="user_meta_image" 
        id="user_meta_image" 
        value="<?php echo esc_url_raw( get_the_author_meta( 'user_meta_image', $user->ID ) ); ?>" class="regular-text" />
    </td>
  </tr>
  <tr>
    <th>
      <label for="user_meta_image_name">Custom image name</label>
    </th>
    <td>
      <input type="text" 
        name="user_meta_image_name" 
        id="user_meta_image_name" 
        value="<?php echo get_the_author_meta('user_meta_image_name', $user->ID); ?>" />
    </td>
  </tr>
  <tr>
    <th>
      <span class="description">
        <?php _e( 'Upload an additional image for your user profile.', 'textdomain' ); ?>
      </span>
    </th>
    <td>
      <input type="hidden" 
        id="user_meta_image_id" 
        name="user_meta_image_id" 
        value="<?php echo get_the_author_meta('user_meta_image_id', $user->ID); ?>" />
    </td>
  </tr>
</table>