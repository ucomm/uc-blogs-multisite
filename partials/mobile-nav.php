<nav id="mobile-nav" role="navigation">
  <?php
    $args = array(
      'theme_location' => 'main',
      'items_wrap' => '<ul id="%1$s" class="nav navbar-nav %2$s">%3$s</ul>',
      'container' => false
    );
    $mobile_menu = wp_nav_menu($args);
  ?>
</nav>