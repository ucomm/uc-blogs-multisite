/******/ (() => { // webpackBootstrap
var __webpack_exports__ = {};
/*!*****************************!*\
  !*** ./src/js/SocMedDir.js ***!
  \*****************************/
var posts = document.getElementsByTagName('article');
var numPosts = posts.length;
var TextFilter = document.getElementById("SocMedTextFilter");
var showClear = false;
var CatFilter = document.getElementById("SocMedCatFilter");
var ClearButton = document.getElementById("ClearFilterBtn");
var ClearBtnContainer = document.getElementById("ClearCntr");

function TextFilterChanged(typed) {
  var searchText = TextFilter.value;

  for (var i = 0; i < numPosts; i++) {
    var id = 'SMdir_title-'.concat(i.toString());
    var title = document.getElementById(id).innerHTML;
    var artID = 'SMdir_post-'.concat(i.toString());
    var selval = document.getElementById("SocMedCatFilter").value;
    var catString = "category-".concat(selval);

    if (!title.toLowerCase().includes(searchText.toLowerCase())) {
      //console.log("Hiding Post ", i);
      document.getElementById(artID).style.display = "none";
    } else if (document.getElementById(artID).className.includes(catString)) {
      document.getElementById(artID).style.display = "block";
    }
  }

  if (typed) {
    document.addEventListener('keyup', function (e) {
      escape_listener(e);
    });
  }

  ToggleClearButton(searchText);
}

function escape_listener(evt) {
  if (evt.keyCode == 27) {
    TextFilter.value = "";
    document.removeEventListener('keyup', function (e) {
      escape_listener(e);
    });
    TextFilterChanged(false);
  } else {
    return;
  }
}

function ToggleClearButton(SearchText) {
  if (SearchText == "" && CatFilter.value == "") {
    ClearBtnContainer.style.display = "none";
  } else {
    ClearBtnContainer.style.display = "block";
  }
}

function ClearAll() {
  CatFilter.value = "";
  TextFilter.value = "";
  document.removeEventListener('keyup', function (e) {
    escape_listener(e);
  });
  TextFilterChanged(false); //ToggleClearButton("");
}

function SelectFilter(sel) {
  var searchText = jQuery("#SocMedTextFilter").val();

  for (var i = 0; i < numPosts; i++) {
    var id = 'SMdir_post-'.concat(i.toString());
    var catString = "category-".concat(sel.value);
    var Title_id = 'SMdir_title-'.concat(i.toString());
    var title = document.getElementById(Title_id).innerHTML;

    if (!document.getElementById(id).className.includes(catString)) {
      document.getElementById(id).style.display = "none";
    } else if (title.toLowerCase().includes(searchText.toLowerCase())) {
      document.getElementById(id).style.display = "block";
    }
  }

  ToggleClearButton(searchText);
}
/******/ })()
;
//# sourceMappingURL=socialMediaDirectory.js.map