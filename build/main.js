/******/ (() => { // webpackBootstrap
/******/ 	"use strict";
/******/ 	var __webpack_modules__ = ({

/***/ "./src/sass/main.scss":
/*!****************************!*\
  !*** ./src/sass/main.scss ***!
  \****************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

__webpack_require__.r(__webpack_exports__);
// extracted by mini-css-extract-plugin


/***/ })

/******/ 	});
/************************************************************************/
/******/ 	// The module cache
/******/ 	var __webpack_module_cache__ = {};
/******/ 	
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/ 		// Check if module is in cache
/******/ 		var cachedModule = __webpack_module_cache__[moduleId];
/******/ 		if (cachedModule !== undefined) {
/******/ 			return cachedModule.exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = __webpack_module_cache__[moduleId] = {
/******/ 			// no module.id needed
/******/ 			// no module.loaded needed
/******/ 			exports: {}
/******/ 		};
/******/ 	
/******/ 		// Execute the module function
/******/ 		__webpack_modules__[moduleId](module, module.exports, __webpack_require__);
/******/ 	
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/ 	
/************************************************************************/
/******/ 	/* webpack/runtime/make namespace object */
/******/ 	(() => {
/******/ 		// define __esModule on exports
/******/ 		__webpack_require__.r = (exports) => {
/******/ 			if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 				Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 			}
/******/ 			Object.defineProperty(exports, '__esModule', { value: true });
/******/ 		};
/******/ 	})();
/******/ 	
/************************************************************************/
var __webpack_exports__ = {};
// This entry need to be wrapped in an IIFE because it need to be isolated against other modules in the chunk.
(() => {
/*!*************************!*\
  !*** ./src/js/index.js ***!
  \*************************/
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _sass_main_scss__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ../sass/main.scss */ "./src/sass/main.scss");

jQuery(document).ready(function ($) {
  // enable accessible main nav menu
  var navigation = new Navigation({
    click: true
  });
  navigation.init(); // enable mobile mmenu navigation when BB is not active
  // this resolves a bug with BB in the responsive mode.

  if (!ajax_obj.is_builder_active) {
    var mobileMenu = $('#mobile-nav').mmenu({
      extensions: ['position-right', 'listview-50', 'border-none']
    });
    var menuIcon = $('#menu-icon');
    var menuApi = mobileMenu.data('mmenu');
    menuIcon.on('click', function () {
      menuApi.open();
    });
    menuApi.bind('open:finish', function () {
      setTimeout(function () {
        menuIcon.addClass('is-active');
      }, 10);
    });
    menuApi.bind('close:finish', function () {
      setTimeout(function () {
        menuIcon.removeClass('is-active');
      }, 10);
    });
  } // handle fetching more posts.


  if (!ajax_obj.is_main_site) {
    var postsContainer = $('.site-posts_container');
    var morePostsBtn = $('.more-posts');
    morePostsBtn.on('click', function () {
      var postsList = $("ul[id^='posts-list']");
      var current_length = postsList.children('li').length + 1;

      if ($('.first__post-container').length) {
        current_length++;
      }

      $.ajax({
        url: ajax_obj.ajaxurl,
        type: 'POST',
        data: {
          action: 'load_posts',
          current_length: current_length
        },
        beforeSend: function beforeSend() {
          morePostsBtn.text('Loading...');
        },
        success: function success(html) {
          if (html == '0') {
            morePostsBtn.detach();
            postsContainer.append('<p class="no-posts">No more posts.</p>');
          } else {
            postsList.append(html);
            postsContainer.append(morePostsBtn);
            morePostsBtn.text('More posts');
          }
        },
        error: function error(req, res, err) {
          var html = "<li><strong class=\"error\">An error occurred while loading the posts.</strong></li>";
          postsList.append(html);
          console.error("AJAX error while fetching posts -> ".concat(err));
          postsContainer.append(morePostsBtn);
          morePostsBtn.text('More posts');
        }
      });
    });
  }
});
})();

/******/ })()
;
//# sourceMappingURL=main.js.map