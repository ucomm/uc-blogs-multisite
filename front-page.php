<?php get_header(); ?>
<main id="content">
  <section <?php
  if (!FLBuilderModel::is_builder_enabled()) {
    echo "class='section-wrapper'";
  }
  ?>>
    <?php 
      if (have_posts()) {
        while (have_posts()) {
          the_post();
          the_content();
        }
      }
    ?>
  </section>
</main>
<?php get_footer(); ?>