<?php 
    get_header();
    $total_posts = UCBlogs\Lib\Helpers::get_total_posts();
    require_once(UC_BLOGS_DIR . '/partials/breadcrumbs.php');
?>

    <main id="content" role="main" aria-label="Content">
        <section class="wrapper section-wrapper">
            <h1 class="category-title">
                <?php 
                    esc_html_e( 'Category: ', 'uc-blogs' ); single_cat_title(); 
                ?>
            </h1>
            <div class='site-posts_container'>
                <ul id="posts-list">
                    <?php 
                        get_template_part('template-parts/content', 'details'); 
                    ?>
                </ul>
                <?php
                    if ($total_posts > $posts_per_page) {
                ?>
                        <button class='more-posts'>More Posts</button>
                <?php
                    }
                ?>
            </div>
        </section>
    </main>

<?php get_footer(); ?>