<?php get_header(); ?>
    <main id="content" role="main" aria-label="Content">
        <section <?php
        if (!FLBuilderModel::is_builder_enabled()) {
            echo "class='section-wrapper'";
        }
        ?>>
            <?php if ( have_posts() ) : while ( have_posts() ) : the_post(); ?>
                <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
                    <?php the_content(); ?>
                </article>
            <?php endwhile; ?>

            <?php else : ?>
                <?php get_template_part('template-parts/content', 'none'); ?>
            <?php endif; ?>
        </section>
    </main>

<?php get_footer(); ?>