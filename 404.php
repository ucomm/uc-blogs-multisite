<?php get_header(); ?>

    <main role="main" aria-label="Content">
        <section class="section-wrapper">
            <article id="post-404">
                <h1><?php esc_html_e( 'Sorry! That page can\'t be found!', 'uc-blogs' ); ?></h1>
                <?php 
                    if (!is_main_site()) {
                        get_search_form(); 
                    }
                ?>
                <p><a class="return-link" href="<?php echo esc_url( home_url() ); ?>"><?php esc_html_e( 'Return to the homepage.', 'uc-blogs' ); ?></a><p>
            </article>
        </section>
    </main>

<?php get_footer(); ?>