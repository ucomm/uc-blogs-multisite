<?php

// Constants
define( 'UC_BLOGS_DIR', get_stylesheet_directory() );
define( 'UC_BLOGS_URL', get_stylesheet_directory_uri() );

if (!defined('OBJECT_STORAGE_URL')) {
  define('OBJECT_STORAGE_URL', 'https://ucommobjectstorage.blob.core.windows.net/uconn-cdn-files');
}

if (!defined('OBJECT_STORAGE_FONTAWESOME_URL')) {
  define('OBJECT_STORAGE_FONTAWESOME_URL', OBJECT_STORAGE_URL . '/shared/icons/legacy-font-awesome-6.4.2');
}


// Load
require_once('vendor/autoload.php');

require_once( 'lib/Bootstrapper.php' );
require_once( 'lib/Customizer.php' );
require_once( 'lib/Helpers.php' );
require_once( 'lib/ShortcodeHandler.php' );


// Bootstrap sites and posts for local development.
$bootstrapper = new UCBlogs\Lib\Bootstrapper();
$is_setup = $bootstrapper->get_uc_blogs_setup();
$host = $bootstrapper->get_server_host();
if (is_multisite() && is_main_site() && !$is_setup && $host === 'localhost') {
  add_action('init', array($bootstrapper, 'init'));
}

// Make Proxima Nova available in Beaver Builder
add_action('init', array('UCBlogs\Lib\Helpers', 'add_custom_fonts'));

// Add Theme Supports
add_theme_support('post-thumbnails');
add_image_size('thumbnail-card', 600, 400);

// Enqueue Scripts and Styles
add_action( 'wp_enqueue_scripts', array('UCBlogs\Lib\Helpers', 'load_scripts') );

// Register Menus and Widgets
add_action('after_setup_theme', array('UCBlogs\Lib\Helpers', 'register_menus'));
add_action('widgets_init', array('UCBlogs\Lib\Helpers', 'register_widgets'));

// Add posts shortcode
add_shortcode('show_posts', 'UCBlogs\Lib\ShortcodeHandler::show_posts');

// AJAX hooks
add_action('wp_ajax_load_posts', array('UCBlogs\Lib\ShortcodeHandler', 'load_posts'));
add_action('wp_ajax_nopriv_load_posts', array('UCBlogs\Lib\ShortcodeHandler', 'load_posts'));

// allow for custom headshots in user profiles.
// - enqueue JS to help with selecting images
add_action('admin_enqueue_scripts', array('UCBlogs\Lib\Helpers', 'load_admin_scripts'));

// - hook into user profiles to create the form part for custom images
add_action('show_user_profile', array('UCBlogs\Lib\Helpers', 'add_headshot'));
add_action('edit_user_profile', array('UCBlogs\Lib\Helpers', 'add_headshot'));

// - hook into updating/saving the user meta data for custom images.
add_action('personal_options_update', array('UCBlogs\Lib\Helpers', 'save_additional_user_meta'));
add_action('edit_user_profile_update', array('UCBlogs\Lib\Helpers', 'save_additional_user_meta'));

// Customizer functions
add_action('customize_register', array('UCBlogs\Lib\Customizer', 'customize_register'));
add_action('customize_save_after', array('UCBlogs\Lib\Customizer', 'compile_scss'));
add_action('wp_head', array('UCBlogs\Lib\Customizer', 'print_css'));

// Filters

// - filter out 'wordpress' from the main site url.
add_filter('option_home', array('UCBlogs\Lib\Helpers', 'update_siteurl'));

// - allow zip and svg upload
add_filter('upload_mimes', array('UCBlogs\Lib\Helpers', 'add_mime_types'));

//Changes made to support social media directory posts:

function create_soc_med_dir_post(){
  $labels = array(
  'name' => __('Social Media Directory Posts'),
  'singular_name' => __('Social Media Directory Post'),
  );
  $args = array(
  'labels' => $labels,
  'public' => true,
  'has_archive' => true,
  'supports' => array(
    'title', 
    'editor', 
    'excerpt', 
    'thumbnail', 
    'custom-fields', 
    'revisions'
  ),
  'taxonomies' => array('category'),
  'rewrite' => array('slug' => 'directory',),
  );
  register_post_type('Social-Media-Dir', $args);
  }
  
  add_action('init', 'create_soc_med_dir_post');