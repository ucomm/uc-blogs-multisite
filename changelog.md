# Changelog
## 1.1.10
- UPdated object storage url from stackpath (rip) to azure
## 1.1.9
- Deprecated direct usage of the UConn banner in favor of generated files from script
## 1.1.8
- Removed meta description from header.php, as it is added via plugin
## 1.1.7
- Added support for fontawesome plugin with fallbacks
## 1.1.6
- Removed /build from excludes.txt
## 1.1.5
- Updated cookie notification path to point to new CDN
## 1.1.4
- Updated font path URLs to point to new CDN
- Updated banner
## 1.1.3
- Composer updates
- Added support for tiktok icon
## 1.1.2
- Composer updates including WP core
## 1.1.1
- Fixed bug in header that could potentially crash sites
## 1.1.0
- Local dev updates
- Dependency updates
## 1.0.10
- Updated customizer
## 1.0.9
- Added styles overrides for contact form 7 forms
## 1.0.8
- Update of dependencies including UConn Banner
## 1.0.7
- Version 2 of social media directory
## 1.0.6
- Implemented social media directory
## 1.0.5
- Updated footer menu styles
## 1.0.4
- Updated nav package/styles
- Update composer packages
## 1.0.3
- Updated nav styles 
- Updated a11y-menu
## 1.0.2
- Registered new menu for footer site list area
- Removed automatic list of sites from footer site list
## 1.0.1
- Added base styles for syntax highlighting
## 1.0.0
- Initial release