<?php get_header(); ?>

    <main id="content" role="main" aria-label="Content">
        <section>
            <h1><?php esc_html_e( 'Index', 'uc-blogs' ); ?></h1>
            <?php get_template_part('template-parts/content', 'loop'); ?>
        </section>
    </main>

<?php get_footer(); ?>