<?php 
    get_header(); 
    require_once(UC_BLOGS_DIR . '/partials/breadcrumbs.php');
?>

    <main role="main" aria-label="Content">
        <section>
            <h1><?php esc_html_e( 'Archive', 'uc-blogs' ); ?></h1>
            <?php get_template_part('template-parts/content', 'loop'); ?>
        </section>
    </main>

<?php get_footer(); ?>