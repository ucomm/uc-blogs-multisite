<?php
use UCBlogs\Lib\Helpers;
?>
            <footer>
                <ul class="social-list">
                    <li class="social-list_item">
                        <a class="link_icon social-list_link" href="https://www.facebook.com/UConn">
                            <?php echo Helpers::get_social_icon('facebook-f'); ?>
                        </a>
                    </li>
                    <li class="social-list_item">
                        <a class="link_icon social-list_link" href="https://www.instagram.com/UConn">
                            <?php echo Helpers::get_social_icon('instagram'); ?>
                        </a>
                    </li>
                    <li class="social-list_item">
                        <a class="link_icon social-list_link" href="https://www.linkedin.com/edu/university-of-connecticut-18031">
                            <?php echo Helpers::get_social_icon('linkedin'); ?>
                        </a>
                    </li>
                    <li class="social-list_item">
                        <a class="link_icon social-list_link" href="https://www.tiktok.com/@uconn?lang=en">
                            <?php echo Helpers::get_social_icon('tiktok'); ?>
                        </a>
                    </li>
                    <li class="social-list_item">
                        <a class="link_icon social-list_link" href="https://www.twitter.com/uconn">
                            <?php echo Helpers::get_social_icon('x-twitter'); ?>
                        </a>
                    </li>
                    <li class="social-list_item">
                        <a class="link_icon social-list_link" href="https://www.youtube.com/user/uconn">
                            <?php echo Helpers::get_social_icon('youtube'); ?>
                        </a>
                    </li>
                </ul>
                <?php
                $site_list_args = array(
                    'container' => 'nav',
                    'menu_id' => 'footer-site-list',
                    'menu_class' => 'sites-list menu',
                    'theme_location' => 'footer-site-list'
                );
                wp_nav_menu($site_list_args);
                ?>
                <?php

                if (is_active_sidebar('footer_widget_1')) {
                    dynamic_sidebar('footer_widget_1');
                }
                if (is_active_sidebar('footer_widget_2')) {
                    dynamic_sidebar('footer_widget_2');
                }

                $args = array(
                    'container' => 'nav',
                    'menu_id' => 'footer-menu',
                    'theme_location' => 'footer'
                );
                wp_nav_menu($args);
                UCBlogs\Lib\Helpers::add_mobile_menu();
                ?>
            </footer>
            </div><!-- /wrapper -->

            <?php wp_footer(); ?>

            </body>

            </html>