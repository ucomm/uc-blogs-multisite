import '../sass/main.scss'

jQuery(document).ready(($) => {

  // enable accessible main nav menu
  const navigation = new Navigation({ click: true });
  navigation.init();

  // enable mobile mmenu navigation when BB is not active
  // this resolves a bug with BB in the responsive mode.
  if (!ajax_obj.is_builder_active) {
    const mobileMenu = $('#mobile-nav').mmenu({
      extensions: [
        'position-right',
        'listview-50',
        'border-none'
      ]
    });
    const menuIcon = $('#menu-icon');
    const menuApi = mobileMenu.data('mmenu');
  
    menuIcon.on('click', () => {
      menuApi.open();
    })
  
    menuApi.bind('open:finish', function () {
      setTimeout(function () {
        menuIcon.addClass('is-active');
      }, 10)
    })
  
    menuApi.bind('close:finish', function () {
      setTimeout(function () {
        menuIcon.removeClass('is-active');
      }, 10)
    })
  }

  // handle fetching more posts.
  if (!ajax_obj.is_main_site) {
    const postsContainer = $('.site-posts_container');
    const morePostsBtn = $('.more-posts');

    morePostsBtn.on('click', () => {
      const postsList = $("ul[id^='posts-list']");
      let current_length = postsList.children('li').length + 1;
      if ($('.first__post-container').length) {
        current_length++;
      }
      $.ajax({
        url: ajax_obj.ajaxurl,
        type: 'POST',
        data: {
          action: 'load_posts',
          current_length
        },
        beforeSend: () => {
          morePostsBtn.text('Loading...');
        },
        success: (html) => {
          if (html == '0') {
            morePostsBtn.detach();
            postsContainer.append('<p class="no-posts">No more posts.</p>');
          } else {
            postsList.append(html);
            postsContainer.append(morePostsBtn);
            morePostsBtn.text('More posts');
          }
        },
        error: (req, res, err) => {
          const html = `<li><strong class="error">An error occurred while loading the posts.</strong></li>`;
          postsList.append(html);
          console.error(`AJAX error while fetching posts -> ${err}`);
          postsContainer.append(morePostsBtn);
          morePostsBtn.text('More posts');
        }
      });
    });
  }

});