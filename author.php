<?php 
  get_header();
  $total_posts = UCBlogs\Lib\Helpers::get_total_posts();
  $author_data = UCBlogs\Lib\Helpers::get_author_data();
  $full_name = $author_data['first_name'] . " " . $author_data['last_name'];
  require_once(UC_BLOGS_DIR . '/partials/breadcrumbs.php');
?>

  <main id="content" role="main" aria-label="Content">
    <section class="wrapper section-wrapper">
      <div class="author__details-container">
        <div class="author__details-image"
          style="background-image: url(<?php echo $author_data['headshot']; ?>);
            background-position: center;
            background-repeat: no-repeat;
            background-size: cover;"></div>
        <h1 class="author__details-name">
          <?php
            echo $full_name;
          ?>
        </h1>
        <p class="author__details-description"><?php echo $author_data['description']; ?></p>
      </div>
      <hr />
      <div class='site-posts_container'>
        <ul id="posts-list">
          <?php 
              get_template_part('template-parts/content', 'details');
          ?>
        </ul>
        <?php
          if ($total_posts > $posts_per_page) {
        ?>
            <button class='more-posts'>More Posts</button>
        <?php
          }
        ?>
      </div>
    </section>
  </main>

<?php get_footer(); ?>