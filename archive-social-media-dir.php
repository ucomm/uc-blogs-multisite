<?php 
    use UCBlogs\Lib\Helpers;
    get_header();
?>  
    <main role="main" aria-label="Content" class="soc_med_dir_main">
        <?php
            $SMTypes = array(
                1 => "Facebook",
                2 => "Instagram",
                3 => "LinkedIn",
                4 => "Snapchat",
                5 => "Twitter",
                6 => "YouTube"
            );

            $IconMap = array(
                1 => 'facebook-f',
                2 => 'instagram',
                3 => 'linkedin-in',
                4 => 'snapchat',
                5 => 'x-twitter',
                6 => 'youtube'
            );

        ?>
        <section>
            <div>
                <h1 id="SocMedDir_Page_Title">Social Media Directory</h1>

                <!-- Filtering -->
                <?php
                    function JoinCategorySets($arr1, $arr2, $IncludedSlugs){
                        $length = count($arr2);
                        $slg = $IncludedSlugs;
                        $FullCats = $arr1;
                        for ($i=0; $i<$length; $i++){
                            if(!in_array($arr2[$i]->slug, $IncludedSlugs)){
                                array_push($FullCats, $arr2[$i]);
                                array_push($slg, $arr2[$i]->slug);
                            }
                        }
                        $val = array($FullCats, $slg);
                        return $val;
                    }
                    function NameCompare($x, $y){
                        return strcmp($x->name, $y->name);
                    }
                    $CategorySet = array();
                    $Slugs = array();
                    query_posts(array( 
                        'post_type' => 'Social-Media-Dir',
                        'showposts' => -1,
                    ) ); 
                    while (have_posts()) : the_post();
                    $ThisPostCategories = array();
                    $ThisPostCategories = get_the_category($post->ID);
                    $retval = JoinCategorySets($CategorySet, $ThisPostCategories, $Slugs);
                    $CategorySet = $retval[0];
                    $Slugs = $retval[1];
                    endwhile;
                    usort($CategorySet, "NameCompare");
                ?>
                <div id="FilterSection">
                    <div id="FilterContainer">
                        <div id="TextFiltContainer">
                            <label for="SocMedTextFilter">Filter by Name: </label>
                            <input type="text" id="SocMedTextFilter" placeholder="Filter by Name" onkeyup="TextFilterChanged(true)">
                        </div>
                        
                        <div id="CatFiltContainer">
                            <label for="SocMedCatFilter">Filter by Category: </label>
                            <select id="SocMedCatFilter" onchange="SelectFilter(this)">
                                <option value=""> Select Category </option>
                                <?php foreach($CategorySet as $cat){  if(($cat->name != "Uncategorized") && ($cat->slug != "featured") && ($cat->slug != 'highlight')){ echo "<option value= '$cat->slug'> $cat->name </option>"; }} ?>
                                <!-- Don't include "Uncategorized", "highlight", or "featured" in the category filter -->
                            </select>
                        </div>
                        
                    </div>
                    <div id="ClearCntr"> <button id="ClearFilterBtn" onclick="ClearAll()">Clear All (X)</button> </div>
                </div>
                <!-- Featured Posts: -->
                <?php 
                    query_posts(array( 
                        'post_type' => 'Social-Media-Dir',
                        'showposts' => -1,
                        'category_name' => 'featured',
                        'orderby' => 'post_date',
                        'order' => 'ASC',
                    ) );  
                ?>

                <!-- SocIndex is used to identify specific posts in the JS code for filtering -->
                <?php $SocIndex=0; ?>
                
                <?php while (have_posts()) : the_post(); ?>
                
                <article id="SMdir_post-<?php echo $SocIndex ?>" <?php post_class(); ?> >
                    <div class="featured_org SMDir_Org">
                    <?php 
                                //Check if there is an excerpt for this post
                                $this_excerpt = get_the_excerpt();
                                if( strlen($this_excerpt) <= 1){?>
                                    <!-- There is no excerpt -->
                                    <!-- Since there is no text below post title, move link icons up to be in line with post title -->
                                    <div class="SMDir_FlexBox_Alternate">
                                        <h3 id="SMdir_title-<?php echo $SocIndex ?>" class="org_title title_no_desc">
                                        <?php $siteurl = get_post_meta($post->ID,"Website",true);
                                            if($siteurl){?>
                                                <a href=<?php echo $siteurl ?>> <?php the_title(); ?> </a>
                                        <?php 
                                        } else {?> 
                                                <?php the_title(); ?>
                                        <?php } ?>
                                        </h3>
                                        <div class="media_acct_links">
                                            <!-- Check for Social Media Account Links -->
                                            <?php
                                            for($x = 1; $x<=6; $x++){
                         
                                                $media = get_post_meta($post->ID, $SMTypes[$x], true);
                                                if($media){?>
                                                    <a class="link_icon" href=<?php echo $media ?>>
                                                        <?php 
                                                        echo Helpers::get_social_icon($IconMap[$x]); ?>
                                                        
                                                    </a>
                                        <?php
                                                }
                                            }
                                        ?>
                                        </div>
                                    </div>
                                <?php
                                } 
                                else{?>
                                    <!-- There is an excerpt -->
                                    <h3 id="SMdir_title-<?php echo $SocIndex ?>" class="org_title">
                                    <?php $siteurl = get_post_meta($post->ID,"Website",true);
                                        if($siteurl){?>
                                            <a href=<?php echo $siteurl ?>> <?php the_title(); ?> </a>
                                    <?php 
                                        } else {?> 
                                                <?php the_title(); ?>
                                    <?php } ?>
                                    </h3>
                                    <div class="SMDir_FlexBox">
                                        <div class="excerpt_cntr">
                                        <?php the_content(); ?>
                                        </div>
                                        <div class="media_acct_links">
                                            <!-- Check for Social Media Account Links -->
                                            <?php
                                            //Loop through each of the 6 social media account types and check if post has taxonomy for that type
                                            for($x = 1; $x<=6; $x++){
                                                $media = get_post_meta($post->ID, $SMTypes[$x], true);
                                                if($media){?>
                                                    <a class="link_icon" href=<?php echo $media ?>>
                                                        <?php echo Helpers::get_social_icon($IconMap[$x]); ?>
                                                        
                                                    </a>
                                        <?php
                                                }
                                            }
                                        ?>
                                        </div>
                                    </div>
                                <?php    
                                }
                            ?>
                    </div>
                </article>
                <?php $SocIndex=$SocIndex+1; ?>
                <?php endwhile;?>

                <!-- Get ID of featured category so it can be used to fetch non-featured posts -->
                <?php $featuredObj = get_category_by_slug('featured'); ?>
                
                <!-- Non-Featured Posts: -->
                <?php 
                    query_posts(array( 
                        'post_type' => 'Social-Media-Dir',
                        'showposts' => -1,
                        'orderby' => 'title',
                        'order' => 'ASC',
                        'tax_query' => array(
                            array(
                                'taxonomy' => 'category',
                                'field'    => 'term_id',
                                'terms'    => array( $featuredObj->term_id ),
                                'operator' => 'NOT IN',
                            ),
                        )
                    ) );  
                ?>
                <?php while (have_posts()) : the_post(); ?>
                    
                    <article id="SMdir_post-<?php echo $SocIndex ?>" <?php post_class(); ?>>
                        <div class="SMDir_Org">
                            <?php 
                            //Check if there is an excerpt for this post
                                $this_excerpt = get_the_excerpt();
                                if( strlen($this_excerpt) <= 1){?>
                                    <!-- There is no excerpt -->
                                    <!-- Since there is no text below post title, move link icons up to be in line with post title -->
                                    <div class="SMDir_FlexBox_Alternate">
                                        <h3 id="SMdir_title-<?php echo $SocIndex ?>" class="org_title title_no_desc">
                                        <?php $siteurl = get_post_meta($post->ID,"Website",true);
                                            if($siteurl){?>
                                                <a href=<?php echo $siteurl ?>> <?php the_title(); ?> </a>
                                        <?php 
                                        } else {?> 
                                                <?php the_title(); ?>
                                        <?php } ?>
                                        </h3>
                                        <div class="media_acct_links">
                                            <!-- Check for Social Media Account Links -->
                                            <?php
                                            for($x = 1; $x<=6; $x++){
                                                $media = get_post_meta($post->ID, $SMTypes[$x], true);
                                                if($media){?>
                                                    <a class="link_icon" href=<?php echo $media ?>>
                                                        <?php echo Helpers::get_social_icon($IconMap[$x]); ?>
                                                        
                                                    </a>
                                        <?php
                                                }
                                            }
                                        ?>
                                        </div>
                                    </div>
                                <?php
                                } 
                                else{?>
                                    <!-- There is an excerpt -->
                                    <h3 id="SMdir_title-<?php echo $SocIndex ?>" class="org_title">
                                    <?php $siteurl = get_post_meta($post->ID,"Website",true);
                                        if($siteurl){?>
                                            <a href=<?php echo $siteurl ?>> <?php the_title(); ?> </a>
                                    <?php 
                                        } else {?> 
                                                <?php the_title(); ?>
                                    <?php } ?>
                                    </h3>
                                    <div class="SMDir_FlexBox">
                                        <div class="excerpt_cntr">
                                        <?php the_content(); ?>
                                        </div>
                                        <div class="media_acct_links">
                                            <!-- Check for Social Media Account Links -->
                                            <?php
                                            //Loop through each of the 6 social media account types and check if post has taxonomy for that type
                                            for($x = 1; $x<=6; $x++){
                                                $media = get_post_meta($post->ID, $SMTypes[$x], true);
                                                if($media){?>
                                                    <a class="link_icon" href=<?php echo $media ?>>
                                                        <?php echo Helpers::get_social_icon($IconMap[$x]); ?>
                                                        
                                                    </a>
                                        <?php
                                                }
                                            }
                                        ?>
                                        </div>
                                    </div>
                                <?php    
                                }
                            ?>
                        </div>
                    </article>
                    <?php $SocIndex=$SocIndex+1; ?>
                <?php endwhile;?>   
            </div>
        </section>
    </main>

<?php get_footer(); ?>