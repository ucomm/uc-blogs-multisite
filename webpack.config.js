const path = require('path')
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const env = process.env.NODE_ENV
const mode = env !== 'production' ? 'development' : 'production'
const buildDir = env !== 'production' ? 'build' : 'dist'

module.exports = {
  entry: {
    main: path.resolve(__dirname, 'src', 'js', 'index.js'),
    socialMediaDirectory: path.resolve(__dirname, 'src', 'js', 'SocMedDir.js'),
    admin: path.resolve(__dirname, 'src', 'admin-js', 'admin.js')
  },
  output: {
    path: path.resolve(__dirname, buildDir),
    filename: '[name].js'
  },
  mode,
  devtool: 'source-map',
  module: {
    rules: [
      {
        test: /\.js$/,
        exclude: /node_modules/,
        use: {
          loader: 'babel-loader',
          options: {
            presets: [
              [
                '@babel/preset-env',
                {
                  debug: true,
                  useBuiltIns: 'entry',
                  corejs: 3.9
                }
              ]
            ]
          }
        }
      },
      {
        test: /\.s?css$/i,
        use: [
          MiniCssExtractPlugin.loader,
          { loader: 'css-loader', options: { sourceMap: true } },
          { loader: 'sass-loader', options: { sourceMap: true } }
        ]
      }
    ]
  },
  resolve: {
    extensions: ['.css', '.js', '.json', '.scss']
  },
  plugins: [
    new MiniCssExtractPlugin({
      filename: '[name].css',
    })
  ]
}